package Algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.common.graph.EndpointPair;
import DataLayouts.AdjEdgeList.AdjGraph;
import ProtoMolecule.graph.DirectedEdge;
import ProtoMolecule.graph.Edge;
import ProtoMolecule.graph.Graph;
import ProtoMolecule.graph.Vertex;

/**
 * Class containing example uses of the protomolecule graph interface. Includes some simple
 * operations and graph algorithms
 */
public class ExampleUsage {

    static Graph graph = new AdjGraph();

    /**
     * Main Testing Program
     * 
     * @param args not taken into account
     */
    public static void main(String[] args) {
        graph.open();

        createGraph(100, 0.2f);

        createAndGetVertex();

        // createAndGetEdge();

        // updateVertexAndEdges();

        // deleteVertexAndEdges();

        // tryLoadML100k();

        // pageRank(100);

        // System.out.println(graph.getAllV().collect(Collectors.toSet()));

        // Vertex maxV = graph.getAllV().max(new Comparator<Vertex>() {

        // @Override
        // public int compare(Vertex o1, Vertex o2) {
        // return ((Double) o1.getProp("pageRank")).compareTo((Double) o2.getProp("pageRank"));
        // }

        // }).get();

        // System.out.println(maxV);

        // findFriends("startingVertex", Set.of("best"), 5);

        graph.close();
    }


    /**
     * This algorithm finds friends of a person in a social graph. You can further specify what the
     * connection (edge label) connecting the two friends should be. Furthermore, you can also
     * specify how far you wish to search from friends of friends.
     * 
     * @param startingVertex the id of the vertex to start the search at
     * @param labels         the edge labels that should be followed in the search (optional); if
     *                       null or empty then all edges are used in the search
     * @param maxDepth       the maximum depth that the search will go to (optional); if null then
     *                       max integer value will be used
     * @return the friends and friends of friends found in the search from the initial person vertex
     */
    public static Set<Vertex> findFriends(String startingVertex, Set<String> labels,
            Integer maxDepth) {

        // setting the edge labels depending on parameter given
        final Set<String> edgeLabels = (labels == null) ? new HashSet<>() : labels;

        // setting max depth to max integer value if none is given
        if (maxDepth == null)
            maxDepth = Integer.MAX_VALUE;

        // neighbours that we have found
        Set<Vertex> neighbours = new HashSet<>();

        // frontier of the search (i.e. the ids of vertices that the next round of search should go
        // from)
        Set<Vertex> frontier = new HashSet<>();
        frontier.add(graph.getV(Stream.of(startingVertex)).findAny().orElseThrow());

        while (maxDepth-- > 0 && !frontier.isEmpty()) {

            // get unvisited neighbours of neighbours
            frontier = frontier.stream().flatMap(v -> v.getNeighbours(1, edgeLabels))
                    .filter(v -> !neighbours.contains(v)).collect(Collectors.toSet());

            neighbours.addAll(frontier);
        }

        return neighbours;
    }



    /**
     * Tests creating and getting a vertex
     */
    public static void createAndGetVertex() {
        System.out.println("Creating first vertex ->");

        Map<String, Object> map = new HashMap<>();
        map.put("age", 20);
        map.put("name", "Nikos");

        graph.addV("person", "1", map);

        System.out.println("Getting the first vertex back ->");
        Vertex v = graph.getAllV().findAny().get();
        System.out.println(v);
    }

    /**
     * tests creating and getting an edge
     */
    public static void createAndGetEdge() {
        System.out.println("Creating vertices...");

        Map<String, Object> map = new HashMap<>();
        map.put("age", 20);
        map.put("name", "Nikos");

        graph.addV("person", "1", map);
        graph.addV("person", "2");

        System.out.println("Creating edge...");

        graph.addE("1", "friends", EndpointPair.ordered("1", "2"), null);

        System.out.print("Getting the edge back -> ");
        Edge e = graph.getAllE().findAny().get();
        System.out.println(e);
    }

    /**
     * tests updating graph entities
     */
    public static void updateVertexAndEdges() {
        System.out.println("Creating vertices...");

        Map<String, Object> map = new HashMap<>();
        map.put("age", 20);
        map.put("name", "Nikos");

        graph.addV("person", "1", map);
        graph.addV("person", "2");

        System.out.println("Creating edge...");

        graph.addE("1", "friends", EndpointPair.ordered("1", "2"), null);

        System.out.println("Swapping direction...");
        DirectedEdge e = (DirectedEdge) graph.getAllE().findAny().get();
        String v1 = e.getFromVertex().getId();
        e.setFromVertex(e.getToVertex().getId());
        e.setToVertex(v1);
        System.out.print("Getting edge -> ");
        Edge e2 = graph.getAllE().findAny().get();
        System.out.println(e2);

        System.out.println("Updating vertex labels...");
        graph.getAllV().forEach(v -> v.setLabel("bros"));
        System.out.println("Getting vertices...");
        graph.getAllV().forEach(v -> System.out.println(v));
    }

    /**
     * tests deleting graph entities
     */
    public static void deleteVertexAndEdges() {
        System.out.println("Creating vertices...");

        Map<String, Object> map = new HashMap<>();
        map.put("age", 20);
        map.put("name", "Nikos");

        graph.addV("person", "1", map);
        graph.addV("person", "2");

        System.out.println("Creating edge...");

        graph.addE("1", "friends", EndpointPair.ordered("1", "2"), null);

        System.out.println("Deleting edge...");
        graph.deleteE(Stream.of("1"));
        boolean x = graph.getE(Stream.of("1")).findAny().isPresent();
        System.out.println("Does edge exist? -> " + x);

        System.out.println("Deleting all vertices...");
        graph.deleteAllV();
        System.out.println("Number of vertices -> " + graph.getAllV().count());
    }

    // bfs optional constraints (labels) (props (edge/vertex) filter)
    /**
     * Breadth First Search (Traversal version) algorithm
     */
    public static void breadthFirstSearch() {
        createGraph();

        Vertex startingVertex =
                graph.getAllV().filter(v -> v.getProp("visited") == null).findAny().orElse(null);

        List<Vertex> vertices = new LinkedList<>();

        // while there are still subgraphs to explore
        while (startingVertex != null) {

            // add starting vertex and set as visited
            startingVertex.setProp("visited", true);
            vertices.add(startingVertex);

            // get all neighbours which haven't been visited yet
            List<Vertex> neighbours = startingVertex.getNeighbours(1, new HashSet<>())
                    .filter(v -> v.getProp("visited") == null).distinct()
                    .collect(Collectors.toList());

            // adding new vertices and marking as visited
            neighbours.stream().forEach(v -> v.setProp("visited", true));
            vertices.addAll(neighbours);

            // while we haven't run out of neighbours to explore
            while (!neighbours.isEmpty()) {

                // get unvisited neighbours of neighbours
                neighbours = neighbours.stream().flatMap(v -> v.getNeighbours(1, new HashSet<>()))
                        .filter(v -> v.getProp("visited") == null).distinct()
                        .collect(Collectors.toList());

                neighbours.stream().forEach(v -> v.setProp("visited", true));
                vertices.addAll(neighbours);
            }

            startingVertex = graph.getAllV().filter(v -> v.getProp("visited") == null).findAny()
                    .orElse(null);
        }

        System.out.println(vertices);
    }

    private static void createGraph() {
        for (int i = 1; i < 11; i++)
            graph.addV("label", Integer.toString(i));

        // creating edges between itself and every other vertex
        // note that they are directed though, so the graph is not complete
        // I also left the final vertex (10) out as an unconnected vertex
        for (int i = 1; i < 10; i++)
            for (int j = i; j < 10; j++)
                graph.addE("label", EndpointPair.ordered(Integer.toString(i), Integer.toString(j)));
    }

    /**
     * 
     * @param numOfVertices the number of vertices that the created graph will have
     * @param chance        the probability an edge will be made; must be between 0.0 and 1.0
     *                      (inclusive)
     */
    private static void createGraph(int numOfVertices, float chance) {
        for (int i = 1; i <= numOfVertices; i++)
            graph.addV("label", Integer.toString(i));


        Random random = new Random();

        // creating edges between itself and every other vertex
        // note that they are directed though, so the graph is not complete
        // I also left the final vertex out as an unconnected vertex
        for (int i = 1; i < numOfVertices; i++)
            for (int j = 1; j < numOfVertices; j++)
                if (random.nextFloat() < chance)
                    graph.addE("label",
                            EndpointPair.ordered(Integer.toString(i), Integer.toString(j)));
    }

    // params -> edge label, name of weight property, x iterations/convergance change limit
    /**
     * Implementation of page rank algorithm using protomolecule graph interface.
     * 
     * @param numIter the number of iterations page rank should run for
     */
    public static void pageRank(int numIter) {

        // initialise page rank values for vertex
        // also initialise numOfNeigbours prop for ease of use later
        double dampingFactor = 0.85;
        double initialPageRankValue = 1.0;
        graph.getAllV().forEach(v -> {
            Map<String, Object> props = new HashMap<>();
            props.put("pageRank", initialPageRankValue);
            props.put("accumulator", 0.0);
            props.put("numOfNeighbours", (double) v.getNumOfNeighbours());
            v.setProps(props);
        });

        // page rank algorithm cycle
        while (numIter-- > 0) {
            // for every vertex, calculate the contribution and add it to neighbours new page rank
            // value
            graph.getAllV().forEach(v -> {
                double pageRankContribution =
                        (double) v.getProp("pageRank") / (double) v.getProp("numOfNeighbours");

                v.getNeighbours(1, new HashSet<>()).forEach(n -> n.setProp("accumulator",
                        (double) n.getProp("accumulator") + pageRankContribution));
            });

            // make new accumulated page rank the page rank value and reset new page rank property
            graph.getAllV().forEach(v -> {
                Map<String, Object> props = v.getAllProps();
                double pageRank =
                        1.0 - dampingFactor + dampingFactor * (double) props.get("accumulator");
                props.replace("pageRank", pageRank);
                props.replace("accumulator", 0.0);
                v.setProps(props);
            });
        }
    }


    /**
     * Loads the ML100K movie ratings dataset into a graph database
     * 
     * @throws IOException if the file does not exist
     */
    public static void loadML100k() throws IOException {
        // get file reader first
        BufferedReader file = new BufferedReader(new FileReader("../data/ml-100k/u.data"));
        String line = file.readLine();

        int count = 0;

        // keep getting lines and making vertices and edges
        while (line != null) {

            if (++count % 10000 == 0) {
                System.out.println(count / 1000 + "%");
            }

            String[] data = line.split("\t");

            // Make user and movie vertices
            if (graph.getV(Stream.of("u-" + data[0])).findAny().isEmpty())
                graph.addV("user", "u-" + data[0]);

            if (graph.getV(Stream.of("m-" + data[1])).findAny().isEmpty())
                graph.addV("movie", "m-" + data[1]);

            // Make ratings edge
            Map<String, Object> info = new HashMap<>();
            info.put("rating", data[2]);
            info.put("timestamp", data[3]);
            graph.addE("rating", EndpointPair.ordered("u-" + data[0], "m-" + data[1]), info);

            line = file.readLine();
        }

        file.close();
    }


    /**
     * Loads the ML100K and handles any exceptions from it. This also may try to query the graph and
     * the rating information within it.
     */
    public static void tryLoadML100k() {
        try {
            loadML100k();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
