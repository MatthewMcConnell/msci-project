/**
 * Various algorithms and usage of the Graph layer API that can be applied to any graph database
 * design.
 */

package Algorithms;
