package DataLayouts.AdjEdgeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import ProtoMolecule.io.Byteable;

/**
 * The byteable vertex value that is to be stored in the data store.
 */
public class VertexValue implements Byteable {

    private String label;
    private Map<String, Object> props;

    /**
     * Creates value with set values provided in the parameters
     * 
     * @param label vertex label
     * @param props vertex properties
     */
    protected VertexValue(String label, Map<String, Object> props) {
        this.label = label;
        this.props = props;
    }

    /**
     * Creates value and deserialises bytes into label and properties
     * 
     * @param bytes serialised value
     */
    protected VertexValue(byte[] bytes) {
        fromBytes(bytes);
    }

    /**
     * 
     * @return vertex label
     */
    protected String getLabel() {
        return this.label;
    }

    /**
     * 
     * @param label vertex label
     */
    protected void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return vertex properties
     */
    protected Map<String, Object> getProps() {
        return this.props;
    }

    /**
     * 
     * @param props vertex properties
     */
    protected void setProps(Map<String, Object> props) {
        this.props = props;
    }


    @Override
    public byte[] toBytes() {

        byte[] bytes;

        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream outputStream = new ObjectOutputStream(byteStream);

            outputStream.writeUTF(label);
            outputStream.writeObject(props);

            bytes = byteStream.toByteArray();

            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Serialisation failed", e);
        }

        return bytes;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void fromBytes(byte[] bytes) {
        try {
            ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
            ObjectInputStream inputStream = new ObjectInputStream(byteStream);

            label = inputStream.readUTF();
            props = (Map<String, Object>) inputStream.readObject();

            inputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Deserialisation failed", e);
        }
    }
}
