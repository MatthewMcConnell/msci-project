package DataLayouts.AdjEdgeList;

import java.io.Serializable;
import java.util.Objects;

/**
 * Forward direction adjacency pair that holds the adjacent edge and destination vertex.
 */
public class AdjacencyPair implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5092285020092309108L;

    /**
     * ID of the adjacent edge
     */
    private String edge;

    /**
     * ID of the adjacent vertex
     */
    private String vertex;


    /**
     * 
     * @param edge   adjacent edge id
     * @param vertex adjacent vertex id
     */
    public AdjacencyPair(String edge, String vertex) {
        this.edge = edge;
        this.vertex = vertex;
    }

    /**
     * 
     * @return adjacent edge id
     */
    public String getEdge() {
        return this.edge;
    }

    /**
     * 
     * @param edge adjacent edge id
     */
    public void setEdge(String edge) {
        this.edge = edge;
    }

    /**
     * 
     * @return adjacent vertex id
     */
    public String getVertex() {
        return this.vertex;
    }

    /**
     * 
     * @param vertex adjacent vertex id
     */
    public void setVertex(String vertex) {
        this.vertex = vertex;
    }

    /**
     * 
     * @param edge adjacent edge id
     * @return adjacency pair
     */
    public AdjacencyPair edge(String edge) {
        setEdge(edge);
        return this;
    }

    /**
     * 
     * @param vertex adjacent vertex id
     * @return adjacency pair
     */
    public AdjacencyPair vertex(String vertex) {
        setVertex(vertex);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AdjacencyPair)) {
            return false;
        }
        AdjacencyPair adjacencyPair = (AdjacencyPair) o;
        return Objects.equals(edge, adjacencyPair.edge)
                && Objects.equals(vertex, adjacencyPair.vertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edge, vertex);
    }

    @Override
    public String toString() {
        return "{" + " edge='" + getEdge() + "'" + ", vertex='" + getVertex() + "'" + "}";
    }

}
