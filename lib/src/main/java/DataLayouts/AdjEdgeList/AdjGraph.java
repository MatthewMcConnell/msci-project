package DataLayouts.AdjEdgeList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.common.graph.EndpointPair;
import DataStore.JavaHashMap.JavaHashMap;
import ProtoMolecule.graph.DirectedEdge;
import ProtoMolecule.graph.Edge;
import ProtoMolecule.graph.EdgeFeature;
import ProtoMolecule.graph.Graph;
import ProtoMolecule.graph.Vertex;
import ProtoMolecule.graph.VertexFeature;
import ProtoMolecule.store.DataItem;
import ProtoMolecule.store.GenericDataStore;

/**
 * Directed graph that uses an edge list model.
 */
public class AdjGraph implements Graph {

    private GenericDataStore vstore = new JavaHashMap();
    private GenericDataStore estore = new JavaHashMap();
    private GenericDataStore astore = new JavaHashMap();


    /**
     * 
     * @return data store for vertices
     */
    protected GenericDataStore getVstore() {
        return this.vstore;
    }

    /**
     * 
     * @return data store for edges
     */
    protected GenericDataStore getEstore() {
        return this.estore;
    }

    /**
     * 
     * @return data store for edges
     */
    protected GenericDataStore getAstore() {
        return this.astore;
    }

    @Override
    public void open() {
        vstore.startUp(null);
        estore.startUp(null);
    }

    @Override
    public void close() {
        vstore.close();
        estore.close();
    }

    @Override
    public void addV(String label, String id, Map<String, Object> props) {
        AdjVertex v = new AdjVertex(this, id, label, props);
        vstore.create(v.getWrappedId(), v.getValue());
    }

    @Override
    public String addV(String label, Map<String, Object> props) {
        AdjVertex v = new AdjVertex(this, null, label, props);

        byte[] id = vstore.create(v.getValue());

        return new StringID(id).getId();
    }

    @Override
    public void addV(String label, String id) {
        AdjVertex v = new AdjVertex(this, id, label, new HashMap<>());
        vstore.create(v.getWrappedId(), v.getValue());
    }

    @Override
    public String addV(String label) {
        AdjVertex v = new AdjVertex(this, null, label, new HashMap<>());

        byte[] id = vstore.create(v.getValue());

        return new StringID(id).getId();
    }

    @Override
    public Stream<Vertex> getV(Stream<String> ids) {
        Stream<DataItem> stream = vstore.read(ids.map(id -> new StringID(id)));

        return stream.filter(d -> d.getValue() != null)
                .map(d -> new AdjVertex(this, d.getId(), d.getValue()));
    }

    @Override
    public Stream<Vertex> getV(String label) {
        return vstore.readAll().map(d -> (Vertex) new AdjVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getLabel().equals(label));
    }

    @Override
    public Stream<Vertex> getV(String prop, Object value) {
        return vstore.readAll().map(d -> (Vertex) new AdjVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getProp(prop).equals(value));
    }

    @Override
    public Stream<Vertex> scanV(String id1, String id2) {
        return vstore.readAll().map(d -> (Vertex) new AdjVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getId().compareTo(id1) >= 0 && v.getId().compareTo(id2) <= 0);
    }

    @Override
    public Stream<Vertex> scanV(String prop, Comparable<Object> value1, Comparable<Object> value2) {
        Stream<Vertex> vertices =
                vstore.readAll().map(d -> (Vertex) new AdjVertex(this, d.getId(), d.getValue()))
                        .filter(v -> v.getProp(prop) != null)
                        .filter(v -> v.getProp(prop).getClass().equals(value1.getClass())
                                && v.getProp(prop).getClass().equals(value1.getClass()))
                        .filter(v -> value1.compareTo(v.getProp(prop)) <= 0
                                && value2.compareTo(v.getProp(prop)) >= 0);

        if (value1 != null) {
            vertices = vertices.filter(v -> v.getProp(prop).getClass().equals(value1.getClass()))
                    .filter(v -> value1.compareTo(v.getProp(prop)) <= 0);
        }

        if (value2 != null) {
            vertices = vertices.filter(v -> v.getProp(prop).getClass().equals(value2.getClass()))
                    .filter(v -> value2.compareTo(v.getProp(prop)) >= 0);
        }

        return vertices;
    }


    @Override
    public Stream<Vertex> getNeighbours(String id, int numHops, Set<String> labels) {

        if (labels.isEmpty())
            return getNeighbours(id, numHops);

        // if the labels aren't empty then we can do a similar algorithm to non-label version but
        // with extra filter stage

        Stream<Vertex> neighbours =
                astore.read(Stream.of(new StringID(id))).filter(d -> d.getValue() != null)
                        .flatMap(d -> new AdjacencyValue(d.getValue()).getPairs().stream())
                        .filter(p -> labels.contains(new AdjEdge(this, p.getEdge()).getLabel()))
                        .map(p -> (Vertex) new AdjVertex(this, p.getVertex())).distinct();

        if (numHops > 1) {
            Stream<Vertex> verticesToExplore = neighbours;

            while (--numHops > 0) {

                verticesToExplore = astore
                        .read(verticesToExplore.map(v -> ((AdjVertex) v).getWrappedId()))
                        .filter(d -> d.getValue() != null)
                        .flatMap(d -> new AdjacencyValue(d.getValue()).getPairs().stream())
                        .filter(p -> labels.contains(new AdjEdge(this, p.getEdge()).getLabel()))
                        .map(p -> (Vertex) new AdjVertex(this, p.getVertex())).distinct();

                neighbours = Stream.concat(neighbours, verticesToExplore);
            }
        }

        return neighbours.distinct();
    }

    @Override
    public Stream<Vertex> getNeighbours(String id, int numHops) {
        // get edges
        // get destination vertices
        // filter vertices so that we are left with only distinct ones
        // add new vertices to previously saved vertices
        // find neighbours from new vertices


        Stream<Vertex> neighbours =
                astore.read(Stream.of(new StringID(id))).filter(d -> d.getValue() != null)
                        .flatMap(d -> new AdjacencyValue(d.getValue()).getPairs().stream())
                        .map(p -> (Vertex) new AdjVertex(this, p.getVertex())).distinct();

        if (numHops > 1) {
            Stream<Vertex> verticesToExplore = neighbours;

            while (--numHops > 0) {

                verticesToExplore =
                        astore.read(verticesToExplore.map(v -> ((AdjVertex) v).getWrappedId()))
                                .filter(d -> d.getValue() != null)
                                .flatMap(d -> new AdjacencyValue(d.getValue()).getPairs().stream())
                                .map(p -> (Vertex) new AdjVertex(this, p.getVertex())).distinct();

                neighbours = Stream.concat(neighbours, verticesToExplore);
            }
        }

        return neighbours.distinct();
    }

    @Override
    public void deleteV(Stream<String> ids) {

        Set<String> idCache = ids.collect(Collectors.toSet());

        vstore.delete(idCache.stream().map(id -> new StringID(id)));
        astore.delete(idCache.stream().map(id -> new StringID(id)));
    }

    @Override
    public void addE(String id, String label, EndpointPair<String> vertices,
            Map<String, Object> props) {

        if (!vertices.isOrdered()) {
            throw new RuntimeException("Only directed edges are allowed in this graph database.");
        }

        AdjEdge e = new AdjEdge(this, id, label, vertices.source(), vertices.target(), props);
        estore.create(e.getWrappedId(), e.getValue());


        DataItem dataItem = astore.read(Stream.of(new StringID(vertices.source()))).findAny().get();

        if (dataItem.getValue() == null) {
            // make a new adjacency list for it
            AdjacencyValue adjacencyValue = new AdjacencyValue();
            adjacencyValue.getPairs().add(new AdjacencyPair(id, vertices.target()));
            astore.create(new StringID(vertices.source()), adjacencyValue);
        } else {
            // add to existing adjacency list
            AdjacencyValue adjacencyValue = new AdjacencyValue(dataItem.getValue());
            adjacencyValue.getPairs().add(new AdjacencyPair(id, vertices.target()));
            astore.update(new StringID(vertices.source()), adjacencyValue);
        }
    }

    @Override
    public String addE(String label, EndpointPair<String> vertices, Map<String, Object> props) {
        AdjEdge edge = new AdjEdge(this, null, label, vertices.source(), vertices.target(), props);
        byte[] id = estore.create(edge.getValue());
        String stringId = new StringID(id).getId();

        DataItem dataItem = astore.read(Stream.of(new StringID(vertices.source()))).findAny().get();

        if (dataItem.getValue() == null) {
            // make a new adjacency list for it
            AdjacencyValue adjacencyValue = new AdjacencyValue();
            adjacencyValue.getPairs().add(new AdjacencyPair(stringId, vertices.target()));
            astore.create(new StringID(vertices.source()), adjacencyValue);
        } else {
            // add to existing adjacency list
            AdjacencyValue adjacencyValue = new AdjacencyValue(dataItem.getValue());
            adjacencyValue.getPairs().add(new AdjacencyPair(stringId, vertices.target()));
            astore.update(new StringID(vertices.source()), adjacencyValue);
        }

        return stringId;
    }

    @Override
    public void addE(String id, String label, EndpointPair<String> vertices) {

        if (!vertices.isOrdered()) {
            throw new RuntimeException("Only directed edges are allowed in this graph database.");
        }

        AdjEdge e =
                new AdjEdge(this, id, label, vertices.source(), vertices.target(), new HashMap<>());
        estore.create(e.getWrappedId(), e.getValue());

        DataItem dataItem = astore.read(Stream.of(new StringID(vertices.source()))).findAny().get();

        if (dataItem.getValue() == null) {
            // make a new adjacency list for it
            AdjacencyValue adjacencyValue = new AdjacencyValue();
            adjacencyValue.getPairs().add(new AdjacencyPair(id, vertices.target()));
            astore.create(new StringID(vertices.source()), adjacencyValue);
        } else {
            // add to existing adjacency list
            AdjacencyValue adjacencyValue = new AdjacencyValue(dataItem.getValue());
            adjacencyValue.getPairs().add(new AdjacencyPair(id, vertices.target()));
            astore.update(new StringID(vertices.source()), adjacencyValue);
        }
    }

    @Override
    public String addE(String label, EndpointPair<String> vertices) {
        AdjEdge edge = new AdjEdge(this, null, label, vertices.source(), vertices.target(),
                new HashMap<>());
        byte[] id = estore.create(edge.getValue());
        String stringId = new StringID(id).getId();

        DataItem dataItem = astore.read(Stream.of(new StringID(vertices.source()))).findAny().get();

        if (dataItem.getValue() == null) {
            // make a new adjacency list for it
            AdjacencyValue adjacencyValue = new AdjacencyValue();
            adjacencyValue.getPairs().add(new AdjacencyPair(stringId, vertices.target()));
            astore.create(new StringID(vertices.source()), adjacencyValue);
        } else {
            // add to existing adjacency list
            AdjacencyValue adjacencyValue = new AdjacencyValue(dataItem.getValue());
            adjacencyValue.getPairs().add(new AdjacencyPair(stringId, vertices.target()));
            astore.update(new StringID(vertices.source()), adjacencyValue);
        }

        return stringId;
    }

    @Override
    public Stream<Edge> getE(Stream<String> edgeIds) {
        Stream<DataItem> stream = estore.read(edgeIds.map(id -> new StringID(id)));

        return stream.filter(d -> d.getValue() != null)
                .map(d -> new AdjEdge(this, d.getId(), d.getValue()));
    }

    @Override
    public Stream<Edge> getE(String label) {
        return estore.readAll().map(d -> new AdjEdge(this, d.getId(), d.getValue()))
                .filter(e -> e.getLabel().equals(label)).map(e -> (Edge) e);
    }

    @Override
    public Stream<Edge> getE(String prop, Object value) {
        return Stream.empty();
    }

    @Override
    public Stream<Edge> scanE(String id1, String id2) {
        return estore.readAll().map(d -> (Edge) new AdjEdge(this, d.getId(), d.getValue()))
                .filter(e -> e.getId().compareTo(id1) >= 0 && e.getId().compareTo(id2) <= 0);
    }

    @Override
    public Stream<Edge> scanE(String prop, Comparable<Object> value1, Comparable<Object> value2) {
        return Stream.empty();
    }

    @Override
    public Stream<Edge> getEAdjacentTo(Set<String> vertexIDs) {

        if (vertexIDs.size() > 2) {
            return Stream.empty();
        }


        // get edges adjacent to first vertex
        Stream<AdjacencyPair> pairs = astore.read(vertexIDs.stream().map(v -> new StringID(v)))
                .filter(d -> d.getValue() != null)
                .flatMap(d -> new AdjacencyValue(d.getValue()).getPairs().stream());


        if (vertexIDs.size() == 2) {
            // remove all vertices with destination vertex not in set
            pairs.filter(p -> vertexIDs.contains(p.getVertex()));
        }

        return pairs.map(p -> new AdjEdge(this, p.getEdge()));
    }

    @Override
    public void deleteE(Stream<String> ids) {
        // collecting it so we can use the stream twice
        Set<String> idCache = ids.collect(Collectors.toSet());

        // remove all adjacency pairs from list
        astore.read(getE(idCache.stream()).map(e -> ((DirectedEdge) e).getFromVertex())
                .map(v -> ((AdjVertex) v).getWrappedId())).forEach(d -> {
                    AdjacencyValue adjacencyValue = new AdjacencyValue(d.getValue());
                    adjacencyValue.getPairs().removeIf(p -> idCache.contains(p.getEdge()));
                    astore.update(new StringID(d.getId()), adjacencyValue);
                });

        // finally delete edge since we no longer need it
        estore.delete(idCache.stream().map(id -> new StringID(id)));
    }

    @Override
    public Stream<Vertex> getAllV() {
        return vstore.readAll().map(d -> new AdjVertex(this, d.getId(), d.getValue()));
    }

    @Override
    public void deleteAllV() {
        vstore.deleteAll();
        astore.deleteAll();
    }

    @Override
    public Stream<Edge> getAllE() {
        return estore.readAll().map(d -> new AdjEdge(this, d.getId(), d.getValue()));
    }

    @Override
    public void deleteAllE() {
        estore.deleteAll();
        astore.deleteAll();
    }

    @Override
    public boolean providesFeature(VertexFeature feature) {
        switch (feature) {
            case LABELS:
            case PROPS:
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean providesFeature(EdgeFeature feature) {
        switch (feature) {
            case IDS:
            case LABELS:
            case DIRECTED:
                return true;
            case UNDIRECTED:
            case PROPS:
                return false;
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "Adjacency/Edge List Graph";
    }
}
