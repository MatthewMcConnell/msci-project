package DataLayouts.AdjEdgeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import ProtoMolecule.io.Byteable;

/**
 * Byteable value for the adjacency list of an origin vertex.
 */
public class AdjacencyValue implements Byteable {

    /**
     * List of adjacency pairs (edge, toVertex)
     */
    private List<AdjacencyPair> pairs = new LinkedList<>();


    /**
     * 
     */
    public AdjacencyValue() {
    }


    /**
     * 
     * @param pairs list of adjacency pairs
     */
    public AdjacencyValue(List<AdjacencyPair> pairs) {
        this.pairs = pairs;
    }

    /**
     * 
     * @param bytes serialised value
     */
    public AdjacencyValue(byte[] bytes) {
        fromBytes(bytes);
    }

    /**
     * 
     * @return list of adjacency pairs
     */
    public List<AdjacencyPair> getPairs() {
        return this.pairs;
    }

    /**
     * 
     * @param pairs list of adjacency pairs
     */
    public void setPairs(List<AdjacencyPair> pairs) {
        this.pairs = pairs;
    }

    /**
     * 
     * @param pairs list of adjacency pairs
     * @return Adjacency value object
     */
    public AdjacencyValue pairs(List<AdjacencyPair> pairs) {
        setPairs(pairs);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AdjacencyValue)) {
            return false;
        }
        AdjacencyValue adjacencyValue = (AdjacencyValue) o;
        return Objects.equals(pairs, adjacencyValue.pairs);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(pairs);
    }

    @Override
    public String toString() {
        return "{" + " pairs='" + getPairs() + "'" + "}";
    }


    @Override
    public byte[] toBytes() {
        byte[] bytes;

        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream outputStream = new ObjectOutputStream(byteStream);

            outputStream.writeObject(pairs);

            bytes = byteStream.toByteArray();

            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Serialisation failed", e);
        }

        return bytes;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void fromBytes(byte[] bytes) {
        try {
            ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
            ObjectInputStream inputStream = new ObjectInputStream(byteStream);

            pairs = (List<AdjacencyPair>) inputStream.readObject();

            inputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Deserialisation failed", e);
        }
    }

}
