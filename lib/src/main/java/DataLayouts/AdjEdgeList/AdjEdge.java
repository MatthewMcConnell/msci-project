package DataLayouts.AdjEdgeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import ProtoMolecule.graph.DirectedEdge;
import ProtoMolecule.graph.Vertex;
import ProtoMolecule.store.DataItem;
import ProtoMolecule.store.GenericDataStore;

/**
 * Directed edge in edge list graph. (Note: this does not support properties)
 */
public class AdjEdge implements DirectedEdge {

    private AdjGraph g;
    private GenericDataStore dataStore;
    private StringID id;
    private EdgeValue value;
    // I'm gonna try this without properties, this should help when trying out solutions in
    // interfaces for features of a graph database

    /**
     * 
     * @param g  graph client instance
     * @param id edge id
     */
    protected AdjEdge(AdjGraph g, String id) {
        this.g = g;
        this.id = new StringID(id);
        this.dataStore = g.getEstore();
    }

    /**
     * 
     * @param g     graph client instance
     * @param id    edge id
     * @param label edge label
     * @param fromV edge from vertex id
     * @param toV   edge to vertex id
     * @param props edge properties
     */
    protected AdjEdge(AdjGraph g, String id, String label, String fromV, String toV,
            Map<String, Object> props) {
        this.g = g;
        this.id = new StringID(id);
        this.dataStore = g.getEstore();
        this.value = new EdgeValue(label, fromV, toV, props);
    }

    /**
     * 
     * @param g     graph client instance
     * @param id    edge id
     * @param value serialised edge value
     */
    protected AdjEdge(AdjGraph g, String id, byte[] value) {
        this.g = g;
        this.id = new StringID(id);
        this.dataStore = g.getEstore();
        this.value = new EdgeValue(value);
    }

    /**
     * 
     * @param g     graph client instance
     * @param id    seriliased edge id
     * @param value serialised edge value
     */
    protected AdjEdge(AdjGraph g, byte[] id, byte[] value) {
        this.g = g;
        this.id = new StringID(id);
        this.dataStore = g.getEstore();
        this.value = new EdgeValue(value);
    }

    /**
     * 
     * @return id wrapped in comparable byteable
     */
    protected StringID getWrappedId() {
        return id;
    }

    /**
     * 
     * @return byteable stored as value
     */
    protected EdgeValue getValue() {
        return value;
    }

    @Override
    public String getId() {
        return id.getId();
    }

    @Override
    public String getLabel() {
        checkAndGetValue();
        return value.getLabel();
    }

    @Override
    public Object getProp(String propName) {
        checkAndGetValue();
        return value.getProps().get(propName);
    }

    @Override
    public Map<String, Object> getProps(Stream<String> propNames) {
        checkAndGetValue();

        Map<String, Object> map = new HashMap<>();

        propNames.forEach(k -> map.put(k, value.getProps().get(k)));

        return map;
    }

    @Override
    public Stream<Vertex> getAdjacentV() {
        Vertex fromVertex = getFromVertex();
        Vertex toVertex = getToVertex();
        return Stream.of(fromVertex, toVertex);
    }

    @Override
    public void setId(String id) {
        checkAndGetValue();

        StringID newID = new StringID(id);

        dataStore.create(newID, value);
        dataStore.delete(Stream.of(this.id));

        // updating the adjacency list with new edge id
        GenericDataStore astore = g.getAstore();

        AdjacencyValue adjacencyValue = new AdjacencyValue(
                astore.read(Stream.of(((AdjVertex) getFromVertex()).getWrappedId())).findAny().get()
                        .getValue());

        adjacencyValue.getPairs().removeIf(p -> p.getEdge().equals(getId()));
        adjacencyValue.getPairs().add(new AdjacencyPair(id, getToVertex().getId()));

        astore.update(((AdjVertex) getFromVertex()).getWrappedId(), adjacencyValue);

        this.id = newID;
    }

    @Override
    public void setLabel(String label) {
        checkAndGetValue();
        value.setLabel(label);
        dataStore.update(id, value);
    }

    @Override
    public void setProp(String propName, Object value) {
        checkAndGetValue();
        this.value.getProps().put(propName, value);
        dataStore.update(id, this.value);
    }

    @Override
    public void setProps(Map<String, Object> props) {
        checkAndGetValue();
        this.value.getProps().putAll(props);
        dataStore.update(id, this.value);
    }

    @Override
    public void deleteProp(String propName) {
        checkAndGetValue();
        value.getProps().remove(propName);
        dataStore.update(id, value);
    }

    @Override
    public void deleteProps(Stream<String> propNames) {
        checkAndGetValue();
        propNames.forEach(key -> value.getProps().remove(key));
        dataStore.update(id, value);
    }

    @Override
    public Vertex getToVertex() {
        checkAndGetValue();
        return new AdjVertex(g, value.getToV());
    }

    @Override
    public Vertex getFromVertex() {
        checkAndGetValue();
        return new AdjVertex(g, value.getFromV());
    }

    @Override
    public void setFromVertex(String fromVertexID) {
        checkAndGetValue();

        // moving the adjacency pair to a new adjacency list
        GenericDataStore astore = g.getAstore();

        AdjacencyValue adjacencyValue = new AdjacencyValue(
                astore.read(Stream.of(((AdjVertex) getFromVertex()).getWrappedId())).findAny().get()
                        .getValue());

        // update adjacency for old vertex
        adjacencyValue.getPairs().removeIf(p -> p.getEdge().equals(getId()));
        astore.update(((AdjVertex) getFromVertex()).getWrappedId(), adjacencyValue);

        // add new adjacency for new vertex
        DataItem dataItem =
                astore.read(Stream.of(new StringID(fromVertexID))).findAny().orElse(null);

        AdjacencyPair newPair = new AdjacencyPair(getId(), getToVertex().getId());
        AdjacencyValue newAdjacencyValue;

        if (dataItem.getValue() == null) {
            List<AdjacencyPair> newList = new ArrayList<>();
            newList.add(newPair);
            newAdjacencyValue = new AdjacencyValue(newList);
        } else {
            newAdjacencyValue = new AdjacencyValue(dataItem.getValue());
            newAdjacencyValue.getPairs().add(newPair);
        }

        astore.create(new StringID(fromVertexID), newAdjacencyValue);


        // final update to the edge value in the estore
        value.setFromV(fromVertexID);
        dataStore.update(id, value);
    }

    @Override
    public void setToVertex(String toVertexID) {
        checkAndGetValue();

        // update pair in origin vertex adjacency list
        GenericDataStore astore = g.getAstore();

        AdjacencyValue adjacencyValue = new AdjacencyValue(
                astore.read(Stream.of(((AdjVertex) getFromVertex()).getWrappedId())).findAny().get()
                        .getValue());

        adjacencyValue.getPairs().removeIf(p -> p.getEdge().equals(getId()));
        adjacencyValue.getPairs().add(new AdjacencyPair(getId(), toVertexID));

        astore.update(((AdjVertex) getFromVertex()).getWrappedId(), adjacencyValue);

        value.setToV(toVertexID);
        dataStore.update(id, value);
    }

    private void checkAndGetValue() {
        if (value == null)
            value = new EdgeValue(dataStore.read(Stream.of(id)).findAny().get().getValue());
    }

    @Override
    public Map<String, Object> getAllProps() {
        checkAndGetValue();
        return value.getProps();
    }

    @Override
    public void deleteAllProps() {
        checkAndGetValue();
        value.getProps().clear();
        dataStore.update(id, value);
    }


    @Override
    public String toString() {
        checkAndGetValue();

        return "{" + "id='" + getId() + "'" + ", label='" + getValue().getLabel() + "'"
                + ", fromV='" + getValue().getFromV() + "'" + ", toV='" + getValue().getToV() + "'"
                + ", props='" + getValue().getProps() + "'" + "}";
    }



    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AdjEdge)) {
            return false;
        }
        AdjEdge testEdge = (AdjEdge) o;
        return getId().equals(testEdge.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
