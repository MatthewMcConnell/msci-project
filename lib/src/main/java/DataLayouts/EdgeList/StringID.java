package DataLayouts.EdgeList;

import java.nio.charset.StandardCharsets;
import ProtoMolecule.io.ComparableByteable;

/**
 * A string ID that can be used as an ID in data stores with conversions to and from bytes (it is
 * byteable).
 */
public class StringID implements ComparableByteable {

    private String id;

    /**
     * 
     * @param id id
     */
    protected StringID(String id) {
        this.id = id;
    }

    /**
     * 
     * @param bytes serialised id
     */
    protected StringID(byte[] bytes) {
        fromBytes(bytes);
    }

    /**
     * 
     * @return id
     */
    protected String getId() {
        return this.id;
    }

    /**
     * 
     * @param id id
     */
    protected void setId(String id) {
        this.id = id;
    }

    @Override
    public byte[] toBytes() {
        return id.getBytes(StandardCharsets.UTF_16);
    }

    @Override
    public void fromBytes(byte[] bytes) {
        id = new String(bytes, StandardCharsets.UTF_16);
    }

    @Override
    public int compareTo(ComparableByteable o) {
        return id.compareTo(new String(o.toBytes(), StandardCharsets.UTF_16));
    }
}
