package DataLayouts.EdgeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import ProtoMolecule.io.Byteable;

/**
 * Byteable value that can be stored in a data store
 */
public class EdgeValue implements Byteable {

    private String label;
    private String fromV;
    private String toV;
    private Map<String, Object> props;

    /**
     * 
     * @param label edge label
     * @param fromV from vertex id
     * @param toV   to vertex id
     * @param props edge properties
     */
    protected EdgeValue(String label, String fromV, String toV, Map<String, Object> props) {
        this.label = label;
        this.fromV = fromV;
        this.toV = toV;
        this.props = props;
    }

    /**
     * Creates the edge value and deserialises bytes to set label and endpoints
     * 
     * @param bytes serialised value
     */
    protected EdgeValue(byte[] bytes) {
        fromBytes(bytes);
    }

    /**
     * 
     * @return vertex label
     */
    protected String getLabel() {
        return this.label;
    }

    /**
     * 
     * @param label edge label
     */
    protected void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return from vertex id
     */
    protected String getFromV() {
        return this.fromV;
    }

    /**
     * 
     * @param fromV from vertex id
     */
    protected void setFromV(String fromV) {
        this.fromV = fromV;
    }

    /**
     * 
     * @return to vertex id
     */
    protected String getToV() {
        return this.toV;
    }

    /**
     * 
     * @param toV edge to vertex id
     */
    protected void setToV(String toV) {
        this.toV = toV;
    }

    /**
     * 
     * @return edge properties
     */
    protected Map<String, Object> getProps() {
        return this.props;
    }

    /**
     * 
     * @param props new edge properties map to be set
     */
    protected void setProps(Map<String, Object> props) {
        this.props = props;
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes;

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();


        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(byteStream);

            outputStream.writeUTF(label);
            outputStream.writeUTF(fromV);
            outputStream.writeUTF(toV);
            outputStream.writeObject(props);

            bytes = byteStream.toByteArray();

            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Serialisation failed", e);
        }

        return bytes;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void fromBytes(byte[] bytes) {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

        try {
            ObjectInputStream inputStream = new ObjectInputStream(byteStream);

            label = inputStream.readUTF();
            fromV = inputStream.readUTF();
            toV = inputStream.readUTF();
            props = (Map<String, Object>) inputStream.readObject();

            inputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Deserialisation failed", e);
        }
    }


}
