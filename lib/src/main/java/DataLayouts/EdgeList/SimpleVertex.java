package DataLayouts.EdgeList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import ProtoMolecule.graph.Graph;
import ProtoMolecule.graph.Vertex;
import ProtoMolecule.store.GenericDataStore;

/**
 * Vertex in Edge List Graph
 */
public class SimpleVertex implements Vertex {

    private Graph graphClient;
    private GenericDataStore dataStore;
    private StringID id;
    private VertexValue value;


    /**
     * 
     * @param graphClient graph client instance
     * @param id          vertex id
     */
    protected SimpleVertex(EdgeListGraph graphClient, String id) {
        this.dataStore = graphClient.getVstore();
        this.id = new StringID(id);
        this.graphClient = graphClient;
    }

    /**
     * 
     * @param graphClient the graph this vertex is meant to be in
     * @param id          the id of the vertex
     * @param label       vertex label
     * @param props       vertex properties
     */
    protected SimpleVertex(EdgeListGraph graphClient, String id, String label,
            Map<String, Object> props) {
        this.dataStore = graphClient.getVstore();
        this.id = new StringID(id);
        this.graphClient = graphClient;
        this.value = new VertexValue(label, props);
    }

    /**
     * 
     * @param graphClient graph client instance
     * @param id          vertex id
     * @param value       serialised vertex value
     */
    protected SimpleVertex(EdgeListGraph graphClient, String id, byte[] value) {
        this.dataStore = graphClient.getVstore();
        this.id = new StringID(id);
        this.graphClient = graphClient;
        this.value = new VertexValue(value);
    }

    /**
     * 
     * @param graphClient graph client instance
     * @param id          serialised vertex id
     * @param value       serialised vertex value
     */
    protected SimpleVertex(EdgeListGraph graphClient, byte[] id, byte[] value) {
        this.dataStore = graphClient.getVstore();
        this.id = new StringID(id);
        this.graphClient = graphClient;
        this.value = new VertexValue(value);
    }

    /**
     * 
     * @return id wrapped in comparable byteable
     */
    protected StringID getWrappedId() {
        return id;
    }

    /**
     * 
     * @return byteable stored as value
     */
    protected VertexValue getValue() {
        return value;
    }

    @Override
    public String getId() {
        return id.getId();
    }

    @Override
    public String getLabel() {

        checkAndGetValue();

        return value.getLabel();
    }

    @Override
    public Object getProp(String propName) {

        checkAndGetValue();

        return value.getProps().get(propName);
    }

    @Override
    public Map<String, Object> getProps(Stream<String> propNames) {
        checkAndGetValue();
        Map<String, Object> map = new HashMap<>();
        propNames.forEach(k -> map.put(k, getProp(k)));
        return map;
    }

    @Override
    public void setId(String id) {

        checkAndGetValue();

        StringID newID = new StringID(id);

        dataStore.create(newID, value);
        dataStore.delete(Stream.of(this.id));

        this.id = newID;
    }

    @Override
    public void setLabel(String label) {
        checkAndGetValue();
        value.setLabel(label);
        dataStore.update(id, value);
    }

    @Override
    public void setProp(String propName, Object value) {
        checkAndGetValue();
        this.value.getProps().put(propName, value);
        dataStore.update(id, this.value);
    }

    @Override
    public void setProps(Map<String, Object> props) {
        checkAndGetValue();
        value.getProps().putAll(props);
        dataStore.update(id, value);
    }

    @Override
    public void deleteProp(String propName) {
        checkAndGetValue();
        value.getProps().remove(propName);
        dataStore.update(id, value);
    }

    @Override
    public void deleteProps(Stream<String> propNames) {
        checkAndGetValue();
        propNames.forEach(p -> value.getProps().remove(p));
        dataStore.update(id, value);
    }

    @Override
    public Stream<Vertex> getNeighbours(int numHops, Set<String> labels) {
        return graphClient.getNeighbours(id.getId(), numHops, labels);
    }

    @Override
    public Stream<Vertex> getNeighbours(int numHops) {
        return graphClient.getNeighbours(id.getId(), numHops, Set.of());
    }

    @Override
    public long getNumOfNeighbours(Set<String> labels) {
        return getNeighbours(1, labels).count();
    }

    @Override
    public long getNumOfNeighbours() {
        return getNeighbours(1).count();
    }

    private void checkAndGetValue() {
        if (value == null) {
            value = new VertexValue(dataStore.read(Stream.of(id)).findAny().get().getValue());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof SimpleVertex)) {
            return false;
        }
        SimpleVertex testVertex = (SimpleVertex) o;
        return getId().equals(testVertex.getId());
    }


    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        checkAndGetValue();

        return "{" + "id='" + getId() + "'" + ", label='" + getValue().getLabel() + "'"
                + ", props='" + getValue().getProps() + "'" + "}";
    }

    @Override
    public Map<String, Object> getAllProps() {
        checkAndGetValue();
        return value.getProps();
    }

    @Override
    public void deleteAllProps() {
        checkAndGetValue();
        value.getProps().clear();
        dataStore.update(id, value);
    }

}
