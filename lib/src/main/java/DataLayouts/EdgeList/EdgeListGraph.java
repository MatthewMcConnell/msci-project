package DataLayouts.EdgeList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import com.google.common.graph.EndpointPair;
import DataStore.JavaHashMap.JavaHashMap;
import ProtoMolecule.graph.Edge;
import ProtoMolecule.graph.EdgeFeature;
import ProtoMolecule.graph.Graph;
import ProtoMolecule.graph.Vertex;
import ProtoMolecule.graph.VertexFeature;
import ProtoMolecule.store.DataItem;
import ProtoMolecule.store.GenericDataStore;

/**
 * Directed graph that uses an edge list model.
 */
public class EdgeListGraph implements Graph {

    private GenericDataStore vstore = new JavaHashMap();
    private GenericDataStore estore = new JavaHashMap();


    /**
     * 
     * @return data store for vertices
     */
    protected GenericDataStore getVstore() {
        return this.vstore;
    }

    /**
     * 
     * @return data store for edges
     */
    protected GenericDataStore getEstore() {
        return this.estore;
    }

    @Override
    public void open() {
        vstore.startUp(null);
        estore.startUp(null);
    }

    @Override
    public void close() {
        vstore.close();
        estore.close();
    }

    @Override
    public void addV(String label, String id, Map<String, Object> props) {
        SimpleVertex v = new SimpleVertex(this, id, label, props);
        vstore.create(v.getWrappedId(), v.getValue());
    }

    @Override
    public String addV(String label, Map<String, Object> props) {
        SimpleVertex v = new SimpleVertex(this, null, label, props);

        byte[] id = vstore.create(v.getValue());

        return new StringID(id).getId();
    }

    @Override
    public void addV(String label, String id) {
        SimpleVertex v = new SimpleVertex(this, id, label, new HashMap<>());
        vstore.create(v.getWrappedId(), v.getValue());
    }

    @Override
    public String addV(String label) {
        SimpleVertex v = new SimpleVertex(this, null, label, new HashMap<>());

        byte[] id = vstore.create(v.getValue());

        return new StringID(id).getId();
    }

    @Override
    public Stream<Vertex> getV(Stream<String> ids) {
        Stream<DataItem> stream = vstore.read(ids.map(id -> new StringID(id)));

        return stream.filter(d -> d.getValue() != null)
                .map(d -> new SimpleVertex(this, d.getId(), d.getValue()));
    }

    @Override
    public Stream<Vertex> getV(String label) {
        return vstore.readAll().map(d -> (Vertex) new SimpleVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getLabel().equals(label));
    }

    @Override
    public Stream<Vertex> getV(String prop, Object value) {
        return vstore.readAll().map(d -> (Vertex) new SimpleVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getProp(prop).equals(value));
    }

    @Override
    public Stream<Vertex> scanV(String id1, String id2) {
        return vstore.readAll().map(d -> (Vertex) new SimpleVertex(this, d.getId(), d.getValue()))
                .filter(v -> v.getId().compareTo(id1) >= 0 && v.getId().compareTo(id2) <= 0);
    }

    @Override
    public Stream<Vertex> scanV(String prop, Comparable<Object> value1, Comparable<Object> value2) {
        Stream<Vertex> vertices =
                vstore.readAll().map(d -> (Vertex) new SimpleVertex(this, d.getId(), d.getValue()))
                        .filter(v -> v.getProp(prop) != null)
                        .filter(v -> v.getProp(prop).getClass().equals(value1.getClass())
                                && v.getProp(prop).getClass().equals(value1.getClass()))
                        .filter(v -> value1.compareTo(v.getProp(prop)) <= 0
                                && value2.compareTo(v.getProp(prop)) >= 0);

        if (value1 != null) {
            vertices = vertices.filter(v -> v.getProp(prop).getClass().equals(value1.getClass()))
                    .filter(v -> value1.compareTo(v.getProp(prop)) <= 0);
        }

        if (value2 != null) {
            vertices = vertices.filter(v -> v.getProp(prop).getClass().equals(value2.getClass()))
                    .filter(v -> value2.compareTo(v.getProp(prop)) >= 0);
        }

        return vertices;
    }


    @Override
    public Stream<Vertex> getNeighbours(String id, int numHops, Set<String> labels) {

        if (labels.isEmpty())
            return getNeighbours(id, numHops);

        // if the labels aren't empty then we can do a similar algorithm to non-label version but
        // with extra filter stage

        Stream<Vertex> neighbours =
                getEAdjacentTo(Set.of(id)).filter(e -> labels.contains(e.getLabel()))
                        .map(e -> ((SimpleEdge) e).getToVertex()).distinct();

        if (numHops > 1) {
            Stream<Vertex> verticesToExplore = neighbours;

            while (--numHops > 0) {

                verticesToExplore =
                        verticesToExplore.flatMap(v -> getEAdjacentTo(Set.of(v.getId())))
                                .filter(e -> labels.contains(e.getLabel()))
                                .map(e -> ((SimpleEdge) e).getToVertex()).distinct();

                neighbours = Stream.concat(neighbours, verticesToExplore);
            }
        }

        return neighbours.distinct();
    }

    @Override
    public Stream<Vertex> getNeighbours(String id, int numHops) {
        // get edges
        // get destination vertices
        // filter vertices so that we are left with only distinct ones
        // add new vertices to previously saved vertices
        // find neighbours from new vertices

        Stream<Vertex> neighbours =
                getEAdjacentTo(Set.of(id)).map(e -> ((SimpleEdge) e).getToVertex()).distinct();


        if (numHops > 1) {
            Stream<Vertex> verticesToExplore = neighbours;

            while (--numHops > 0) {

                verticesToExplore =
                        verticesToExplore.flatMap(v -> getEAdjacentTo(Set.of(v.getId())))
                                .map(e -> ((SimpleEdge) e).getToVertex()).distinct();

                neighbours = Stream.concat(neighbours, verticesToExplore);
            }
        }

        return neighbours.distinct();
    }

    @Override
    public void deleteV(Stream<String> ids) {
        vstore.delete(ids.map(id -> new StringID(id)));
    }

    @Override
    public void addE(String id, String label, EndpointPair<String> vertices,
            Map<String, Object> props) {

        if (!vertices.isOrdered()) {
            throw new RuntimeException("Only directed edges are allowed in this graph database.");
        }

        SimpleEdge e = new SimpleEdge(this, id, label, vertices.source(), vertices.target(), props);
        estore.create(e.getWrappedId(), e.getValue());
    }

    @Override
    public String addE(String label, EndpointPair<String> vertices, Map<String, Object> props) {
        SimpleEdge edge =
                new SimpleEdge(this, null, label, vertices.source(), vertices.target(), props);
        byte[] id = estore.create(edge.getValue());

        return new StringID(id).getId();
    }

    @Override
    public void addE(String id, String label, EndpointPair<String> vertices) {

        if (!vertices.isOrdered()) {
            throw new RuntimeException("Only directed edges are allowed in this graph database.");
        }

        SimpleEdge e = new SimpleEdge(this, id, label, vertices.source(), vertices.target(),
                new HashMap<>());
        estore.create(e.getWrappedId(), e.getValue());


    }

    @Override
    public String addE(String label, EndpointPair<String> vertices) {
        SimpleEdge edge = new SimpleEdge(this, null, label, vertices.source(), vertices.target(),
                new HashMap<>());
        byte[] id = estore.create(edge.getValue());

        return new StringID(id).getId();
    }

    @Override
    public Stream<Edge> getE(Stream<String> edgeIds) {
        Stream<DataItem> stream = estore.read(edgeIds.map(id -> new StringID(id)));

        return stream.filter(d -> d.getValue() != null)
                .map(d -> new SimpleEdge(this, d.getId(), d.getValue()));
    }

    @Override
    public Stream<Edge> getE(String label) {
        return estore.readAll().map(d -> new SimpleEdge(this, d.getId(), d.getValue()))
                .filter(e -> e.getLabel().equals(label)).map(e -> (Edge) e);
    }

    @Override
    public Stream<Edge> getE(String prop, Object value) {
        return Stream.empty();
    }

    @Override
    public Stream<Edge> scanE(String id1, String id2) {
        return estore.readAll().map(d -> (Edge) new SimpleEdge(this, d.getId(), d.getValue()))
                .filter(e -> e.getId().compareTo(id1) >= 0 && e.getId().compareTo(id2) <= 0);
    }

    @Override
    public Stream<Edge> scanE(String prop, Comparable<Object> value1, Comparable<Object> value2) {
        return Stream.empty();
    }

    @Override
    public Stream<Edge> getEAdjacentTo(Set<String> vertexIDs) {

        if (vertexIDs.size() > 2) {
            return Stream.empty();
        }

        // get all edges
        Stream<Edge> stream =
                estore.readAll().map(d -> (Edge) new SimpleEdge(this, d.getId(), d.getValue()));

        // get edges that have an adjacent vertex matching the vertex id 1
        stream = stream.filter(e -> e.getAdjacentV().anyMatch(v -> vertexIDs.contains(v.getId())));

        if (vertexIDs.size() == 2) {
            // get remaining edges that have an adjacent vertex matching the vertex id 2
            stream = stream
                    .filter(e -> e.getAdjacentV().anyMatch(v -> vertexIDs.contains(v.getId())));
        }

        return stream;
    }

    @Override
    public void deleteE(Stream<String> ids) {
        estore.delete(ids.map(id -> new StringID(id)));
    }

    @Override
    public Stream<Vertex> getAllV() {
        return vstore.readAll().map(d -> new SimpleVertex(this, d.getId(), d.getValue()));
    }

    @Override
    public void deleteAllV() {
        vstore.deleteAll();
    }

    @Override
    public Stream<Edge> getAllE() {
        return estore.readAll().map(d -> new SimpleEdge(this, d.getId(), d.getValue()));
    }

    @Override
    public void deleteAllE() {
        estore.deleteAll();
    }

    @Override
    public boolean providesFeature(VertexFeature feature) {
        switch (feature) {
            case LABELS:
            case PROPS:
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean providesFeature(EdgeFeature feature) {
        switch (feature) {
            case IDS:
            case LABELS:
            case DIRECTED:
                return true;
            case UNDIRECTED:
            case PROPS:
                return false;
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "Edge List Graph";
    }
}
