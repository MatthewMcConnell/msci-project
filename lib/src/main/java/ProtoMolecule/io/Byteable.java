package ProtoMolecule.io;

/**
 * Represents a value that is able to be converted to and from a Byte[].
 * 
 * Analoguous to the Hadoop Writable interface.
 */
public interface Byteable {
    // hehe, is your class 'BYTE'able... Ok bad joke
    // somehow I managed to come up with a cannabilistic sounding type

    /**
     * Serialises the value into bytes
     * 
     * @return serialised value
     */
    byte[] toBytes();


    /**
     * Deserialises bytes into the value
     * 
     * @param bytes serialised value
     */
    void fromBytes(byte[] bytes);
}
