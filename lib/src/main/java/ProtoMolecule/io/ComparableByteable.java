package ProtoMolecule.io;

/**
 * Represents a serialisable value that can also be compared.
 */
public interface ComparableByteable extends Byteable, Comparable<ComparableByteable> {
    // should this be comparable to a general type?
}
