/**
 * Contains interfaces for data to be useful in the IO between the representation layer and the data
 * storage layer in a protomolecule graph database.
 */
package ProtoMolecule.io;
