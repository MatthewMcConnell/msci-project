package ProtoMolecule.graph;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import com.google.common.graph.EndpointPair;

/**
 * Represents functions that store components of a graph into a data store.
 */
public interface Graph {

        /**
         * Starts all necessary components of the graph database to allow for operations to begin.
         */
        public void open();

        /**
         * Shutdown of the graph database with no further operations allowed to be executed on the
         * instance.
         */
        public void close();

        /**
         * Says whether a graph feature is provided by the implementation or not.
         * 
         * @param feature vertex specific feature that you want to check is provided
         * @return true if the feature is provided, false otherwise
         */
        public boolean providesFeature(VertexFeature feature);

        /**
         * Says whether a graph feature is provided by the implementation or not.
         * 
         * @param feature edge specific feature that you want to check is provided
         * @return true if the feature is provided, false otherwise
         */
        public boolean providesFeature(EdgeFeature feature);

        /**
         * Creates a vertex in the graph
         * 
         * @param label the label or type of the vertex
         * @param id    vertex id
         * @param props map of the properties the vertex will have
         */
        public void addV(String label, String id, Map<String, Object> props);

        /**
         * Creates a vertex in the graph
         * 
         * @param label vertex label
         * @param props map of properties and values the vertex should have
         * @return assigned id of the newly created vertex
         */
        public String addV(String label, Map<String, Object> props);

        /**
         * Creates a vertex in the graph
         * 
         * @param label the label or type of the vertex
         * @param id    vertex id
         */
        public void addV(String label, String id);

        /**
         * Creates a vertex in the graph
         * 
         * @param label vertex label
         * @return assigned id of the newly created vertex
         */
        public String addV(String label);

        /**
         * Retrieves vertices with certain ids
         * 
         * @param ids vertex ids of edges to return
         * @return vertices with matching ids
         */
        public Stream<Vertex> getV(Stream<String> ids);

        /**
         * Retrieves all vertices
         * 
         * @return all vertices stored in the graph database
         */
        public Stream<Vertex> getAllV();

        /**
         * Retrieves vertices with a matching label type
         * 
         * @param label vertex label
         * @return vertices with matching label
         */
        public Stream<Vertex> getV(String label);

        /**
         * Gets vertices matching the property value.
         * 
         * @param prop  property name
         * @param value if null is provided then vertices that do not have the property are returned
         * @return vertices matching the property value
         */
        public Stream<Vertex> getV(String prop, Object value);

        /**
         * Gets vertices within a range (inclusive)
         * 
         * @param id1 starting vertex id
         * @param id2 ending vertex id
         * @return vertices with ids within the range
         */
        public Stream<Vertex> scanV(String id1, String id2);

        // maybe I need to add a label type here... or at least the option for one
        /**
         * Gets vertices with a property value in a range of values (inclusive).
         * 
         * If the object types do not match the property type of the vertex then the vertex is
         * excluded.
         * 
         * @param prop   property name
         * @param value1 value to start scanning from; if null is provided then minimum value is
         *               assumed
         * @param value2 value to stop scanning (inclusive); if null is provided then maximum value
         *               is assumed
         * @return vertices with property values within the range of value1 and value2 inclusive
         */
        public Stream<Vertex> scanV(String prop, Comparable<Object> value1,
                        Comparable<Object> value2);

        /**
         * Gets all neighbours within a number of hops (neighbourhood)
         * 
         * @param id      id of vertex that neighbours should be connected to
         * @param numHops number of neighbour hops that should be done in the search
         * @param labels  labels of edges that are to be followed; if none are provided then all
         *                edge types are followed
         * @return vertex neighbours at hop distance/from 1 to hop distance
         */
        public Stream<Vertex> getNeighbours(String id, int numHops, Set<String> labels);

        /**
         * Gets all neighbours within a number of hops (neighbourhood)
         * 
         * @param id      id of vertex that neighbours should be connected to
         * @param numHops number of neighbour hops that should be done in the search
         * @return vertex neighbours at hop distance/from 1 to hop distance
         */
        public Stream<Vertex> getNeighbours(String id, int numHops);

        /**
         * 
         * @param ids vertex ids
         */
        public void deleteV(Stream<String> ids);

        /**
         * 
         */
        public void deleteAllV();

        /**
         * Creates an edge in the graph. The edge can be undirected or directed and depends on
         * whether the endpoint pair provided is undirected/directed.
         * 
         * @param id       edge id
         * @param label    the label or type of the edge
         * @param vertices the pair of vertices that the edge is adjacent to; can be directed or
         *                 undirected
         * @param props    map of the properties the edge will have
         */
        public void addE(String id, String label, EndpointPair<String> vertices,
                        Map<String, Object> props);

        /**
         * Creates an edge in the graph. The edge can be undirected or directed and depends on
         * whether the endpoint pair provided is undirected/directed.
         * 
         * @param label    edge label
         * @param vertices the pair of vertices that the edge is adjacent to; can be directed or
         *                 undirected
         * @param props    edge properties
         * @return assigned id of the newly created edge
         */
        public String addE(String label, EndpointPair<String> vertices, Map<String, Object> props);

        /**
         * Creates an edge in the graph. The edge can be undirected or directed and depends on
         * whether the endpoint pair provided is undirected/directed.
         * 
         * @param id       edge id
         * @param label    the label or type of the edge
         * @param vertices the pair of vertices that the edge is adjacent to; can be directed or
         *                 undirected
         */
        public void addE(String id, String label, EndpointPair<String> vertices);

        /**
         * Creates an edge in the graph. The edge can be undirected or directed and depends on
         * whether the endpoint pair provided is undirected/directed.
         * 
         * @param label    edge label
         * @param vertices the pair of vertices that the edge is adjacent to; can be directed or
         *                 undirected
         * @return assigned id of the newly created edge
         */
        public String addE(String label, EndpointPair<String> vertices);

        /**
         * Retrieves edges with certain ids
         * 
         * @param edgeIds edge ids of edges to return
         * @return edges with matching ids
         */
        public Stream<Edge> getE(Stream<String> edgeIds);

        /**
         * Retrieves all edges
         * 
         * @return all edges stored in the graph database
         */
        public Stream<Edge> getAllE();

        /**
         * Retrieves edges with a matching label type
         * 
         * @param label edge label to match; if none are provided then all edges are returned
         * @return edges with matching label
         */
        public Stream<Edge> getE(String label);

        /**
         * Gets edges matching the property value.
         * 
         * @param prop  property name
         * @param value if null is provided then vertices that do not have the property are returned
         * @return edges matching the property value
         */
        public Stream<Edge> getE(String prop, Object value);

        // not really sure about the use of this with edges
        /**
         * Gets edges within an inclusive range
         * 
         * @param id1 starting edge id
         * @param id2 ending edge id
         * @return edges with ids within the range
         */
        public Stream<Edge> scanE(String id1, String id2);

        /**
         * Gets edges with a property value in a range of values (inclusive)
         * 
         * @param prop   property name
         * @param value1 value to start scanning from; if null is provided then minimum value is
         *               assumed
         * @param value2 value to stop scanning (inclusive); if null is provided then maximum value
         *               is assumed
         * @return edges with property values within the range of value1 and value2 inclusive
         */
        public Stream<Edge> scanE(String prop, Comparable<Object> value1,
                        Comparable<Object> value2);


        /**
         * Gets edges that are incident with all vertex ids provided
         * 
         * @param vertexIDs vertices that the edges should be adjacent to
         * @return edges incident with all vertex ids
         */
        public Stream<Edge> getEAdjacentTo(Set<String> vertexIDs);

        /**
         * 
         * @param ids edge ids to delete
         */
        public void deleteE(Stream<String> ids);

        /**
         * 
         */
        public void deleteAllE();
}
