package ProtoMolecule.graph;

import java.util.stream.Stream;

/**
 * Represents a directed hyperedge.
 */
public interface DirectedHyperEdge extends Edge {

    /**
     * 
     * @return from vertex of the edge
     */
    public Stream<Vertex> getFromVertices();

    /**
     * 
     * @return to vertex of the edge
     */
    public Stream<Vertex> getToVertices();


    /**
     * 
     * @param fromVertexID from vertex of edge to be set
     */
    public void addFromVertex(String fromVertexID);

    /**
     * 
     * @param fromVertexIDs from vertex of edge to be set
     */
    public void addFromVertices(Stream<String> fromVertexIDs);

    /**
     * 
     * @param toVertexID from vertex of edge to be set
     */
    public void addToVertex(String toVertexID);

    /**
     * 
     * @param toVertexIDs from vertex of edge to be set
     */
    public void addToVertices(Stream<String> toVertexIDs);

    /**
     * 
     * @param fromVertexID from vertex of edge to be removed
     */
    public void removeFromVertex(String fromVertexID);

    /**
     * 
     * @param fromVertexIDs from vertices of edge to be removed
     */
    public void removeFromVertices(Stream<String> fromVertexIDs);

    /**
     * 
     * @param toVertexID from vertex of edge to be removed
     */
    public void removeToVertex(String toVertexID);

    /**
     * 
     * @param toVertexIDs from vertices of edge to be removed
     */
    public void removeToVertices(Stream<String> toVertexIDs);
}
