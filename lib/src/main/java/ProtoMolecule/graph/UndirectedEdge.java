package ProtoMolecule.graph;

/**
 * Represents a undirected edge adjacent to two vertices.
 */
public interface UndirectedEdge extends Edge {

    /**
     * 
     * @param oldVertexID vertex to be replaced
     * @param newVertexID vertex to replace with
     */
    public void replaceVertex(String oldVertexID, String newVertexID);
}
