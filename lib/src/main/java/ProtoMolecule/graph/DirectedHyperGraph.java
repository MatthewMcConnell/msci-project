package ProtoMolecule.graph;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Represents a directed hypergraph.
 */
public interface DirectedHyperGraph extends DirectedGraph {

        /**
         * Creates a directed hyperedge in the graph
         * 
         * @param id    edge id
         * @param label the label or type of the edge
         * @param fromV the id of the from vertices
         * @param toV   the id of the to vertices
         * @param props map of the properties the edge will have
         */
        public void addE(String id, String label, Stream<String> fromV, Stream<String> toV,
                        Map<String, Object> props);

        /**
         * Creates a directed hyperedge in the graph
         * 
         * @param label edge label
         * @param fromV the id of the from vertices
         * @param toV   the id of the to vertices
         * @param props edge properties
         * @return assigned id of the newly created edge
         */
        public String addE(String label, Stream<String> fromV, Stream<String> toV,
                        Map<String, Object> props);

        /**
         * Creates a directed hyperedge in the graph
         * 
         * @param id    edge id
         * @param label the label or type of the edge
         * @param fromV the id of the from vertices
         * @param toV   the id of the to vertices
         */
        public void addE(String id, String label, Stream<String> fromV, Stream<String> toV);

        /**
         * Creates a directed hyperedge in the graph
         * 
         * @param label edge label
         * @param fromV the id of the from vertices
         * @param toV   the id of the to vertices
         * @return assigned id of the newly created edge
         */
        public String addE(String label, Stream<String> fromV, Stream<String> toV);

        /**
         * 
         * @param vertexIDs id of vertices that edges start at
         * @return edges that begin from all the vertices
         */
        public Stream<DirectedHyperEdge> getEWithFromV(Set<String> vertexIDs);

        /**
         * 
         * @param vertexIDs id of vertices that retrieved edges end at
         * @return edges that end at all the vertices
         */
        public Stream<DirectedHyperEdge> getEWithToV(Set<String> vertexIDs);

}
