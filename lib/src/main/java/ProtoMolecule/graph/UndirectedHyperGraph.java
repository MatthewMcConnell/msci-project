package ProtoMolecule.graph;

import java.util.Map;
import java.util.stream.Stream;

/**
 * Represents an undirected hypergraph.
 */
public interface UndirectedHyperGraph extends Graph {

    /**
     * Creates an undirected hyperedge in the graph
     * 
     * @param id        edge id
     * @param label     the label or type of the edge
     * @param vertexIDs the ids of vertices adjacent to the edge
     * @param props     map of the properties the edge will have
     */
    public void addE(String id, String label, Stream<String> vertexIDs, Map<String, Object> props);

    /**
     * Creates an undirected hyperedge in the graph
     * 
     * @param label     edge label
     * @param vertexIDs the ids of vertices adjacent to the edge
     * @param props     edge properties
     * @return assigned id of the newly created edge
     */
    public String addE(String label, Stream<String> vertexIDs, Map<String, Object> props);

    /**
     * Creates an undirected hyperedge in the graph
     * 
     * @param id        edge id
     * @param label     the label or type of the edge
     * @param vertexIDs the ids of vertices adjacent to the edge
     */
    public void addE(String id, String label, Stream<String> vertexIDs);

    /**
     * Creates an undirected hyperedge in the graph
     * 
     * @param label     edge label
     * @param vertexIDs the ids of vertices adjacent to the edge
     * @return assigned id of the newly created edge
     */
    public String addE(String label, Stream<String> vertexIDs);
}
