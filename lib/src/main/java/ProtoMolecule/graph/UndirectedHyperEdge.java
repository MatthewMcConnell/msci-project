package ProtoMolecule.graph;

import java.util.stream.Stream;

/**
 * Represents an undirected hyperedge.
 */
public interface UndirectedHyperEdge extends UndirectedEdge {

    /**
     * 
     * @param vId vertex id
     */
    public void addAdjacentV(String vId);

    /**
     * 
     * @param vId vertex to be no longer adjacent
     */
    public void removeAdjacentV(String vId);

    /**
     * 
     * @param vIds vertex ids
     */
    public void addAdjacentV(Stream<String> vIds);

    /**
     * 
     * @param vIds vertices to be no longer adjacent
     */
    public void removeAdjacentV(Stream<String> vIds);
}
