package ProtoMolecule.graph;

import java.util.stream.Stream;

/**
 * Represents a directed graph.
 */
public interface DirectedGraph extends Graph {

    /**
     * Gets edges whose from vertex matches the id
     * 
     * @param vertexID id of vertex that edges start at
     * @return edges that begin from the specified vertex
     */
    public Stream<DirectedEdge> getEWithFromV(String vertexID);

    /**
     * 
     * @param vertexID id of vertex that retrieved edges end at
     * @return edges that end at the specified vertex
     */
    public Stream<DirectedEdge> getEWithToV(String vertexID);

}
