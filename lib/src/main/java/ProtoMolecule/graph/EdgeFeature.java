package ProtoMolecule.graph;

/**
 * Represents edge features that a graph database can provide (e.g. edges that can have properties).
 */
public enum EdgeFeature {
    /** Edges have ids */
    IDS,
    /** Edges have labels */
    LABELS,
    /** Edges have properties */
    PROPS,
    /** Edges can be directed */
    DIRECTED,
    /** Edges can be undirected */
    UNDIRECTED
}
