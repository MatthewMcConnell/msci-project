package ProtoMolecule.graph;

/**
 * Represents a directed edge.
 */
public interface DirectedEdge extends Edge {

    /**
     * 
     * @return to vertex of the edge
     */
    public Vertex getToVertex();

    /**
     * 
     * @return from vertex of the edge
     */
    public Vertex getFromVertex();

    /**
     * 
     * @param fromVertexID from vertex of edge to be set
     */
    public void setFromVertex(String fromVertexID);

    /**
     * 
     * @param toVertexID from vertex of edge to be set
     */
    public void setToVertex(String toVertexID);
}
