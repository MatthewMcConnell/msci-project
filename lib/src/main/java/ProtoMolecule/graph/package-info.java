/**
 * Includes all interfaces for the graph representation layer within a protomolecule database.
 */
package ProtoMolecule.graph;
