package ProtoMolecule.graph;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Represents a labeled property vertex.
 */
public interface Vertex {

    /**
     * 
     * @return vertex id
     */
    public String getId();

    /**
     * 
     * @return vertex label
     */
    public String getLabel();

    /**
     * 
     * @param propName the property value to retrieve
     * @return property value; null if the vertex does not have the property
     */
    public Object getProp(String propName);

    /**
     * 
     * @param propNames properties to retrieve
     * @return vertex properties
     */
    public Map<String, Object> getProps(Stream<String> propNames);

    /**
     * 
     * @return all vertex properties
     */
    public Map<String, Object> getAllProps();


    /**
     * 
     * @param id vertex id to be set
     */
    public void setId(String id);

    /**
     * 
     * @param label vertex label to be set
     */
    public void setLabel(String label);

    /**
     * 
     * @param propName the name of the property you wish to set
     * @param value    the value of the property
     */
    public void setProp(String propName, Object value);

    /**
     * Multi-set property operation that will add (or update if it already exists) properties
     * 
     * @param props map of property names and values to set
     */
    public void setProps(Map<String, Object> props);

    /**
     * 
     * @param propName the property to delete
     */
    public void deleteProp(String propName);

    /**
     * 
     * @param propNames properties to delete; if none are provided then all are deleted
     */
    public void deleteProps(Stream<String> propNames);

    /**
     * 
     */
    public void deleteAllProps();

    /**
     * Gets all neighbours within a number of hops (neighbourhood)
     * 
     * @param numHops number of neighbour hops that should be done in the search
     * @param labels  labels of edges that are to be followed; if none are provided then all edge
     *                types are followed
     * @return vertex neighbours at hop distance/from 1 to hop distance
     */
    public Stream<Vertex> getNeighbours(int numHops, Set<String> labels);

    /**
     * Gets all neighbours within a number of hops (neighbourhood)
     * 
     * @param numHops number of neighbour hops that should be done in the search
     * 
     * @return vertex neighbours at hop distance/from 1 to hop distance
     */
    public Stream<Vertex> getNeighbours(int numHops);

    /**
     * Gets the number of neighbours the vertex is connected to. Note that direction is taken into
     * account. This will not include vertices connected where this vertex is the destination.
     * 
     * @param labels the labels edges should have to the vertices neighbours
     * @return number of vertices that the vertex is connected to
     */
    public long getNumOfNeighbours(Set<String> labels);

    /**
     * Gets the number of neighbours the vertex is connected to. Note that direction is taken into
     * account. This will not include vertices connected where this vertex is the destination.
     * 
     * @return number of vertices that the vertex is connected to
     */
    public long getNumOfNeighbours();
}
