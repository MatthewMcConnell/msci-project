package ProtoMolecule.graph;

import java.util.Map;
import java.util.stream.Stream;

/**
 * Represents a labeled property edge.
 */
public interface Edge {

    /**
     * 
     * @return edge id
     */
    public String getId();

    /**
     * 
     * @return edge label
     */
    public String getLabel();

    /**
     * 
     * @param propName the property value to retrieve
     * @return property value; null if the vertex does not have the property
     */
    public Object getProp(String propName);

    /**
     * 
     * @param propNames properties to retrieve
     * @return vertex properties
     */
    public Map<String, Object> getProps(Stream<String> propNames);

    /**
     * 
     * @return vertex properties
     */
    public Map<String, Object> getAllProps();

    /**
     * 
     * @return all vertices adjacent to the edge
     */
    public Stream<Vertex> getAdjacentV();

    /**
     * 
     * @param id edge id
     */
    public void setId(String id);

    /**
     * 
     * @param label edge label
     */
    public void setLabel(String label);

    /**
     * 
     * @param propName the name of the property you wish to set
     * @param value    the value of the property
     */
    public void setProp(String propName, Object value);

    /**
     * Multi-set property operation that will add (or update if it already exists) properties
     * 
     * @param props map of property names and values to set
     */
    public void setProps(Map<String, Object> props);

    /**
     * 
     * @param propName the property to delete
     */
    public void deleteProp(String propName);

    /**
     * 
     * @param propNames properties to delete
     */
    public void deleteProps(Stream<String> propNames);

    /**
     * 
     */
    public void deleteAllProps();
}
