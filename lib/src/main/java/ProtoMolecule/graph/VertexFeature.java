package ProtoMolecule.graph;

/**
 * Represents vertex features that a graph database can provide (e.g. vertices that can have
 * properties).
 */
public enum VertexFeature {
    /** Vertices have labels */
    LABELS,
    /** Vertices have properties */
    PROPS
}
