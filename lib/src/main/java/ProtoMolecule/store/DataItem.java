package ProtoMolecule.store;

import java.util.Objects;

/**
 * Represents a data item in a data store that has an byte[] and value.
 */
public class DataItem {

    private byte[] id;
    private byte[] value;

    /**
     * 
     * @param id    binary id
     * @param value binary value
     */
    public DataItem(byte[] id, byte[] value) {
        this.id = id;
        this.value = value;
    }

    /**
     * 
     * @return binary id
     */
    public byte[] getId() {
        return this.id;
    }

    /**
     * 
     * @param id binary id
     */
    public void setId(byte[] id) {
        this.id = id;
    }

    /**
     * 
     * @return binary value
     */
    public byte[] getValue() {
        return this.value;
    }

    /**
     * 
     * @param value binary value
     */
    public void setValue(byte[] value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof DataItem)) {
            return false;
        }
        DataItem dataItem = (DataItem) o;
        return Objects.equals(id, dataItem.id) && Objects.equals(value, dataItem.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value);
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", value='" + getValue() + "'" + "}";
    }


}
