package ProtoMolecule.store;

import java.util.stream.Stream;
import ProtoMolecule.io.Byteable;
import ProtoMolecule.io.ComparableByteable;

/**
 * Represents a data store of any type.
 */
public interface GenericDataStore {

    /**
     * Gets the data store ready for commands and to be operational.
     * 
     * @param url connection url to a data store
     */
    public void startUp(String url);

    /**
     * Ends any client session and generally cleans up any connections or memory taken (like a
     * deconstructor).
     */
    public void close();


    /**
     * Adds a data value with a specified id
     * 
     * @param id    data id
     * @param value data value
     */
    public void create(ComparableByteable id, Byteable value);

    /**
     * Adds a data value and automatically assigns an id
     * 
     * @param value data value
     * @return assigned data id
     */
    public byte[] create(Byteable value);

    /**
     * 
     * @param ids ids of data to retrieve
     * @return data items with values if it exists at the id, otherwise null is returned for an id
     */
    public Stream<DataItem> read(Stream<ComparableByteable> ids);

    /**
     * 
     * @return all data items stored in the data store
     */
    public Stream<DataItem> readAll();


    /**
     * Updates an ids value if it already exists
     * 
     * @param id    data to update
     * @param value updated data
     */
    public void update(ComparableByteable id, Byteable value);

    /**
     * 
     * @param ids data to delete
     */
    public void delete(Stream<ComparableByteable> ids);

    /**
     * 
     */
    public void deleteAll();
}
