package DataStore.JavaHashMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;
import ProtoMolecule.io.Byteable;
import ProtoMolecule.io.ComparableByteable;
import ProtoMolecule.store.DataItem;
import ProtoMolecule.store.GenericDataStore;

/**
 * Data store implementation for use of a java hashmap.
 */
public class JavaHashMap implements GenericDataStore {

    Map<ByteWrapper, byte[]> store = new HashMap<>();

    @Override
    public void startUp(String url) {
        // could use the url as a file to load hashmap from
    }

    @Override
    public void close() {
        // could make this save a file
    }

    @Override
    public void create(ComparableByteable id, Byteable value) {
        store.put(new ByteWrapper(id.toBytes()), value.toBytes());
    }

    @Override
    public byte[] create(Byteable value) {
        Random rand = new Random();
        byte[] key = new byte[8];
        while (store.containsKey(new ByteWrapper(key))) {
            rand.nextBytes(key);
        }

        store.put(new ByteWrapper(key), value.toBytes());
        return key;
    }

    @Override
    public Stream<DataItem> read(Stream<ComparableByteable> ids) {
        return ids.map(id -> new DataItem(id.toBytes(), store.get(new ByteWrapper(id.toBytes()))));
    }

    @Override
    public void update(ComparableByteable id, Byteable value) {
        store.replace(new ByteWrapper(id.toBytes()), value.toBytes());
    }

    @Override
    public void delete(Stream<ComparableByteable> ids) {
        ids.forEach(id -> store.remove(new ByteWrapper(id.toBytes())));
    }

    @Override
    public Stream<DataItem> readAll() {
        return store.entrySet().stream()
                .map(e -> new DataItem(e.getKey().getBytes(), e.getValue()));
    }

    @Override
    public void deleteAll() {
        store.clear();
    }
}
