package DataStore.JavaHashMap;

import java.util.Arrays;

/**
 * Wrapper for byte array so that they can be used in hashmap
 */
public class ByteWrapper {

    private final byte[] bytes;

    /**
     * 
     * @param bytes bytes to be wrapped
     */
    protected ByteWrapper(byte[] bytes) {
        this.bytes = bytes;
    }

    /**
     * 
     * @return bytes contained in the wrapper
     */
    public byte[] getBytes() {
        return this.bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ByteWrapper)) {
            return false;
        }
        ByteWrapper byteWrapper = (ByteWrapper) o;
        return Arrays.equals(bytes, byteWrapper.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

}
