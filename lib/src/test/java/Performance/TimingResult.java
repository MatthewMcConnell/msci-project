package Performance;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TimingResult {

    private List<Long> times = new ArrayList<>();
    private Long average;

    public TimingResult(List<Long> times) {
        this.times = times;
        calculateAverage();
    }

    public List<Long> getTimes() {
        return this.times;
    }

    public void setTimes(List<Long> times) {
        this.times = times;
        calculateAverage();
    }

    public Long getAverage() {
        return this.average;
    }

    private void calculateAverage() {
        this.average = times.stream().reduce(0L, Long::sum) / times.size();
    }

    public TimingResult times(List<Long> times) {
        setTimes(times);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TimingResult)) {
            return false;
        }
        TimingResult timingResult = (TimingResult) o;
        return Objects.equals(times, timingResult.times)
                && Objects.equals(average, timingResult.average);
    }

    @Override
    public int hashCode() {
        return Objects.hash(times, average);
    }

    @Override
    public String toString() {
        return "{" + " average='" + getAverage() + "'" + "}";
    }

}
