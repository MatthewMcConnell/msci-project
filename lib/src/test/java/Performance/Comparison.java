package Performance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import com.google.common.graph.EndpointPair;
import DataLayouts.AdjEdgeList.AdjGraph;
import DataLayouts.EdgeList.EdgeListGraph;
import ProtoMolecule.graph.Graph;

public class Comparison {

    public static Map<String, TimingResult> timingRun(Consumer<Graph> setUp, Consumer<Graph> method,
            Consumer<Graph> cleanUp, Graph... graphs) {

        Map<String, TimingResult> results = new HashMap<>(graphs.length);

        for (Graph graph : graphs) {

            List<Long> times = new ArrayList<>(10);

            for (int i = 0; i < 10; i++) {

                // System.out.println(graph + ": run " + i);

                setUp.accept(graph);

                long start = System.nanoTime();
                method.accept(graph);
                long end = System.nanoTime();

                cleanUp.accept(graph);

                times.add(end - start);
            }

            results.put(graph.toString(), new TimingResult(times));
        }

        return results;
    }

    /**
     * will create a graph number of vertices and random number of edges per node based on the
     * chance param given
     */
    private static void createGraph(Graph graph, int numOfVertices, float chance) {
        for (int i = 1; i <= numOfVertices; i++)
            graph.addV("label", Integer.toString(i));


        Random random = new Random();

        // creating edges between itself and every other vertex
        // note that they are directed though, so the graph is not complete
        // I also left the final vertex out as an unconnected vertex
        for (int i = 1; i < numOfVertices; i++)
            for (int j = 1; j < numOfVertices; j++)
                if (random.nextFloat() < chance)
                    graph.addE("label",
                            EndpointPair.ordered(Integer.toString(i), Integer.toString(j)));
    }

    private static void createVertices(Graph graph, int numOfVertices) {
        for (int i = 1; i <= numOfVertices; i++)
            graph.addV("label", Integer.toString(i));
    }

    private static void createEdges(Graph graph, int numOfVertices, float chance) {
        Random random = new Random();

        for (int i = 1; i < numOfVertices; i++)
            for (int j = 1; j < numOfVertices; j++)
                if (random.nextFloat() < chance)
                    graph.addE("label",
                            EndpointPair.ordered(Integer.toString(i), Integer.toString(j)));

    }

    private static void deleteGraph(Graph graph) {
        graph.deleteAllV();
        graph.deleteAllE();
    }

    private static void doNothing(Graph graph) {
    }

    /**
     * Implementation of page rank algorithm using protomolecule graph interface.
     * 
     * @param numIter the number of iterations page rank should run for
     */
    private static void pageRank(Graph graph, int numIter) {

        // initialise page rank values for vertex
        // also initialise numOfNeigbours prop for ease of use later
        double dampingFactor = 0.85;
        double initialPageRankValue = 1.0;
        graph.getAllV().forEach(v -> {
            Map<String, Object> props = new HashMap<>();
            props.put("pageRank", initialPageRankValue);
            props.put("accumulator", 0.0);
            props.put("numOfNeighbours", (double) v.getNumOfNeighbours());
            v.setProps(props);
        });

        // page rank algorithm cycle
        while (numIter-- > 0) {
            // for every vertex, calculate the contribution and add it to neighbours new page rank
            // value
            graph.getAllV().forEach(v -> {
                double pageRankContribution =
                        (double) v.getProp("pageRank") / (double) v.getProp("numOfNeighbours");

                v.getNeighbours(1, new HashSet<>()).forEach(n -> n.setProp("accumulator",
                        (double) n.getProp("accumulator") + pageRankContribution));
            });

            // make new accumulated page rank the page rank value and reset new page rank property
            graph.getAllV().forEach(v -> {
                Map<String, Object> props = v.getAllProps();
                double pageRank =
                        1.0 - dampingFactor + dampingFactor * (double) props.get("accumulator");
                props.replace("pageRank", pageRank);
                props.replace("accumulator", 0.0);
                v.setProps(props);
            });
        }
    }

    public static void main(String[] args) {

        Graph adjGraph = new AdjGraph();
        Graph edgeGraph = new EdgeListGraph();

        System.out.println("Starting Performance Comparison\n");

        System.out.println("\nCreate Graph");
        System.out.println(timingRun(Comparison::doNothing, (g -> createGraph(g, 1000, 0.01f)),
                Comparison::deleteGraph, edgeGraph, adjGraph));

        System.out.println("\nDelete Graph");
        System.out.println(timingRun((g -> createGraph(g, 1000, 0.01f)), Comparison::deleteGraph,
                Comparison::doNothing, edgeGraph, adjGraph));

        System.out.println("\nCreate Vertices");
        System.out.println(timingRun(Comparison::doNothing, (g -> createVertices(g, 1000)),
                Comparison::deleteGraph, edgeGraph, adjGraph));

        System.out.println("\nCreate Edges");
        System.out.println(timingRun((g -> createVertices(g, 1000)),
                (g -> createEdges(g, 1000, 0.01f)), Comparison::deleteGraph, edgeGraph, adjGraph));

        System.out.println("\nPageRank");
        System.out.println(timingRun((g -> createGraph(g, 100, 0.01f)), (g -> pageRank(g, 1)),
                Comparison::deleteGraph, edgeGraph, adjGraph));

        System.out.println("Finished Performance Comparison\n");
    }
}
