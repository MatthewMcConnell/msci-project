# ProtoMolecule

ProtoMolecule is a graph database prototyping tool. This tool allows users to create components for a graph database that can interact with other layers of the graph database.

The layers we currently represent in a graph database are:
- **Graph** - the representation or data layout of the graph database, e.g. adjacency list design
  - In this layer, implementations decide how a vertex or edge is stored and retrieved in the database
- **Store** - the data storage of the graph database, e.g. key-value store (like a hashmap)
  - In this layer, implementations map common operations such as read and write to a specific data storage backend

The idea behind ProtoMolecule is to provide interfaces that capture the common behaviour of components of graph databases. Thus, by implementing these interfaces, you will be able to change the components used in a graph database design easily. It should also allow you to create new designs quickly without creating an entire graph database from scratch.

Once a graph database is created, you can use it through the graph interface API.

Here is a diagram of how ProtoMolecule is structured at a high-level:
![Diagram of how ProtoMolecule is Structured at a High-Level](https://gitlab.com/MatthewMcConnell/msci-project/-/wikis/imgs/protomolecule-current.png "Diagram of how ProtoMolecule is Structured at a High-Level")

Currently, we have two graph representation designs in the `DataLayouts` package - adjacency edge list (`AdjEdgeList`) and edge list (`EdgeList`). Both only provide simple directed edges but also allow vertices and edges to have labels (type) and properties.

We also have one data storage implementation in the `DataStore.JavaHashMap` package. It implements a driver so that a Java hashmap can be used as a backend data store. It is used by the test design as the data storage of choice.

Furthermore, we have an `ExampleUsage.java` class in the `Algorithms` package that showcases the usage of the test graph database and the graph API it implements.

To investigate this tool further, have a look at our [documentation](https://matthewmcconnell.gitlab.io/msci-project/)!

In case more guidance is needed, here is what a typical graph model looks like in a graph database (includes vertices, edges, labels, and properties):
![Typical Graph Model in a Graph Database](https://dist.neo4j.com/wp-content/uploads/modeling_johnsally_properties.jpg "Typical Graph Model in a Graph Database")

### Note to anyone working on this in the future

I have noticed quite late on in the project that conistency was never really discussed or considered when creating the interface API. Thus, I realised there might be issues in the two designs I've currently created. One known issue is that (some or all) adjacent edges are not deleted when a vertex is deleted. Therefore, it might be worth considering and clarifying this when further developing the tool. But there is plenty of other further work detailed in my paper to do as well.